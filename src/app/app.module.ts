import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { Feature1Module } from './feature1/feature1.module';
import { LoaderComponent } from './loader/loader.component';
import { SharedModule } from './shared/shared.module';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { assetUrl } from '../single-spa/asset-url';
import { NgxMaskModule } from 'ngx-mask';
import { PopoverModule } from 'ngx-bootstrap/popover';
@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    AppLayoutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    Feature1Module,
    AppRoutingModule,
    SharedModule,
    CoreModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http, assetUrl('localization/'), '.json');
        },
        deps: [HttpClient]
      }
    }),
    NgxMaskModule.forRoot(),
    PopoverModule.forRoot()
  ],
  providers: [],
  exports: [
    //always empty, since you don't want to import AppModule to any other module
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
