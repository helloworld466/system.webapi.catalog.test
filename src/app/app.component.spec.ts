import { TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
describe('AppComponent', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));
  describe('Generic Validation', () => {
    it('should create the app', () => {
      // Arrange
      const fixture = TestBed.createComponent(AppComponent);
      // Act
      const app = fixture.componentInstance;
      // Aseert
      expect(app).toBeTruthy();
    });
  });
});
