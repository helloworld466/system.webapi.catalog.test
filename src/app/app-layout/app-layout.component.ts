import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { NgOption, NgSelectComponent } from '@ng-select/ng-select';
import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from '../core/config.service';
@Component({
  selector: 'mf-template-app-root',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.css']
})
export class AppLayoutComponent implements OnInit, AfterViewInit {
  options: NgOption[] = [
    { id: 'en-us', name: 'English (US)' },
    { id: 'en-ca', name: 'English (CA)' },
    { id: 'fr-ca', name: 'Francais (CA)' },
    { id: 'de-de', name: 'Deutsch (DE)' },
    { id: 'es-es', name: 'Español (ES)' }
  ];
  @ViewChild('myselect')
  myselect: NgSelectComponent;
  private locale: string;
  constructor(private configService: ConfigService, private translateService: TranslateService) { }
  ngOnInit() {
    this.locale = this.configService.locale;
    this.translateService.setDefaultLang('en-us');
    this.translateService.use(this.locale);
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.myselect.select(this.myselect.itemsList.items.find(i => i.value === this.locale));
    });
  }
  langChanged(locale: string) {
    this.configService.locale = locale;
    this.translateService.use(locale);
  }
}
