import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AppLayoutComponent } from './app-layout.component';
xdescribe('AppLayoutComponent', () => {
  let component: AppLayoutComponent;
  let fixture: ComponentFixture<AppLayoutComponent>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AppLayoutComponent]
    }).compileComponents();
  }));
  describe('Generic Validation', () => {
    it('should create the app', () => {
      // Arrange
      fixture = TestBed.createComponent(AppLayoutComponent);
      // Act
      component = fixture.componentInstance;
      // Aseert
      expect(component).toBeTruthy();
    });
  });
});
