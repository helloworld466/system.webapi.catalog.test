import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { Feature1MainPageComponent } from './feature1-main-page/feature1-main-page.component';
import { Feature1Page1Component } from './feature1-page1/feature1-page1.component';
import { Feature1Page2Component } from './feature1-page2/feature1-page2.component';
import { Feature1Service } from './feature1.service';
@NgModule({
  declarations: [
    Feature1MainPageComponent,
    Feature1Page1Component,
    Feature1Page2Component
  ],
  imports: [
    SharedModule
  ],
  providers: [
    Feature1Service
  ]
})
export class Feature1Module { }
