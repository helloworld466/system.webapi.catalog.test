import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Feature1MainPageComponent } from './feature1-main-page.component';
xdescribe('Feature1MainPageComponent', () => {
  let component: Feature1MainPageComponent;
  let fixture: ComponentFixture<Feature1MainPageComponent>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Feature1MainPageComponent]
    }).compileComponents();
  }));
  describe('Generic Validation', () => {
    it('should create', () => {
      // Arrange
      fixture = TestBed.createComponent(Feature1MainPageComponent);
      // Act
      component = fixture.componentInstance;
      // Assert
      expect(component).toBeTruthy();
    });
  });
});
