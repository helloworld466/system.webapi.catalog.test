import { Component, OnInit } from '@angular/core';
import { UserDataSource } from '../user-datasource';
import { Feature1Service } from '../feature1.service';
@Component({
  selector: 'my-app-feature1-main-page',
  templateUrl: './feature1-main-page.component.html',
  styleUrls: ['./feature1-main-page.component.css']
})
export class Feature1MainPageComponent implements OnInit {
  dataSource: UserDataSource;
  displayedColumns = [
    'id',
    'firstName',
    'lastName',
    'unit'
  ]
  constructor(private feature1Service: Feature1Service) { }
  ngOnInit() {
    this.dataSource = new UserDataSource(this.feature1Service)
  }
}
