import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpService } from "../core/http.service";
import { User } from "./models";
@Injectable()
export class Feature1Service {
  constructor(private httpService: HttpService) { }
  getUsers(): Observable<User[]> {
    return this.httpService.get<User[]>('users');
  }
}
