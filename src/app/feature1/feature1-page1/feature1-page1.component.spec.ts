import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Feature1Page1Component } from './feature1-page1.component';
xdescribe('Feature1Page1Component', () => {
  let component: Feature1Page1Component;
  let fixture: ComponentFixture<Feature1Page1Component>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Feature1Page1Component]
    }).compileComponents();
  }));
  describe('Generic Validation', () => {
    it('should create', () => {
      // Arrange
      fixture = TestBed.createComponent(Feature1Page1Component);
      // Act
      component = fixture.componentInstance;
      // Assert
      expect(component).toBeTruthy();
    });
  });
});
