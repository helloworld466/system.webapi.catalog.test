import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable } from "rxjs";
import { Feature1Service } from "./feature1.service";
import { User } from "./models";
export class UserDataSource implements DataSource<User>{
  constructor(private feature1Service: Feature1Service) { }
  connect(collectionViewer: CollectionViewer): Observable<User[] | readonly User[]> {
    return this.feature1Service.getUsers();
  }
  disconnect(collectionViewer: CollectionViewer): void {
  }
}
