import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Feature1Page2Component } from './feature1-page2.component';
xdescribe('Feature1Page2Component', () => {
  let component: Feature1Page2Component;
  let fixture: ComponentFixture<Feature1Page2Component>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Feature1Page2Component]
    }).compileComponents();
  }));
  describe('Generic Validation', () => {
    it('should create', () => {
      // Arrange
      fixture = TestBed.createComponent(Feature1Page2Component);
      // Act
      component = fixture.componentInstance;
      // Assert
      expect(component).toBeTruthy();
    });
  });
});
