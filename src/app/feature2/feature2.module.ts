import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { Feature2MainPageComponent } from './feature2-main-page/feature2-main-page.component';
import { Feature2Page1Component } from './feature2-page1/feature2-page1.component';
import { Feature2Page2Component } from './feature2-page2/feature2-page2.component';
import { Feature2RoutingModule } from './feature2-routing.module';
import { Feature2Service } from './feature2.service';
@NgModule({
  declarations: [
    Feature2MainPageComponent,
    Feature2Page1Component,
    Feature2Page2Component
  ],
  imports: [
    Feature2RoutingModule,
    SharedModule
  ],
  providers: [
    Feature2Service
  ]
})
export class Feature2Module { }
