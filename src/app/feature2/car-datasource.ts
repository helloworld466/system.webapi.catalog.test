import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable } from "rxjs";
import { Feature2Service } from "./feature2.service";
import { Car } from "./models";
export class CarDataSource implements DataSource<Car>{
  constructor(private feature2Service: Feature2Service) { }
  connect(collectionViewer: CollectionViewer): Observable<Car[] | readonly Car[]> {
    return this.feature2Service.getCars();
  }
  disconnect(collectionViewer: CollectionViewer): void {
  }
}
