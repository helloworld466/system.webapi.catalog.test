import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Feature2MainPageComponent } from './feature2-main-page.component';
xdescribe('Feature1mainpageComponent', () => {
  let component: Feature2MainPageComponent;
  let fixture: ComponentFixture<Feature2MainPageComponent>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Feature2MainPageComponent]
    }).compileComponents();
  }));
  describe('Generic Validation', () => {
    it('should create', () => {
      // Arrange
      fixture = TestBed.createComponent(Feature2MainPageComponent);
      // Act
      component = fixture.componentInstance;
      // Assert
      expect(component).toBeTruthy();
    });
  });
});
