import { Component, OnInit } from '@angular/core';
import { CarDataSource } from '../car-datasource';
import { Feature2Service } from '../feature2.service';
@Component({
  selector: 'my-app-feature2-main-page',
  templateUrl: './feature2-main-page.component.html',
  styleUrls: ['./feature2-main-page.component.css']
})
export class Feature2MainPageComponent implements OnInit {
  dataSource: CarDataSource;
  displayedColumns = [
    'id',
    'make',
    'model',
    'countryOfOrigin'
  ]
  constructor(private feature2Service: Feature2Service) { }
  ngOnInit() {
    this.dataSource = new CarDataSource(this.feature2Service)
  }
}
