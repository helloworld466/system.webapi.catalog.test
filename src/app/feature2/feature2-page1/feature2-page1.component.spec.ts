import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Feature2Page1Component } from './feature2-page1.component';
xdescribe('Feature2Page1Component', () => {
  let component: Feature2Page1Component;
  let fixture: ComponentFixture<Feature2Page1Component>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Feature2Page1Component]
    }).compileComponents();
  }));
  describe('Generic Validation', () => {
    it('should create', () => {
      // Arrange
      fixture = TestBed.createComponent(Feature2Page1Component);
      // Act
      component = fixture.componentInstance;
      // Assert
      expect(component).toBeTruthy();
    });
  });
});
