import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Feature2Page2Component } from './feature2-page2.component';
xdescribe('Feature2Page2Component', () => {
  let component: Feature2Page2Component;
  let fixture: ComponentFixture<Feature2Page2Component>;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Feature2Page2Component]
    }).compileComponents();
  }));
  describe('Generic Validation', () => {
    it('should create', () => {
      // Arrange
      fixture = TestBed.createComponent(Feature2Page2Component);
      // Act
      component = fixture.componentInstance;
      // Assert
      expect(component).toBeTruthy();
    });
  });
});
