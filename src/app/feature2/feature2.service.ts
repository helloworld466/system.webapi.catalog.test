import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../core/http.service';
import { Car } from './models';
@Injectable()
export class Feature2Service {
  constructor(private httpService: HttpService) { }
  getCars(): Observable<Car[]> {
    return this.httpService.get<Car[]>('cars');
  }
}
