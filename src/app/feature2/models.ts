export class Car {
  id: string;
  make: string;
  model: string;
  countryOfOrigin: string;
}
