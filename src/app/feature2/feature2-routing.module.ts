import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Feature2MainPageComponent } from './feature2-main-page/feature2-main-page.component';
import { Feature2Page1Component } from './feature2-page1/feature2-page1.component';
import { Feature2Page2Component } from './feature2-page2/feature2-page2.component';
const routes: Routes = [
  { path: '', component: Feature2MainPageComponent },
  { path: 'page1', component: Feature2Page1Component },
  { path: 'page2', component: Feature2Page2Component }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Feature2RoutingModule { }
