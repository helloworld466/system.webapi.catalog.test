import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, publish, refCount, scan, startWith } from 'rxjs/operators';
@Injectable()
export class LoaderService {
  isLoading: Observable<boolean>;
  private loadingSubject: Subject<number>;
  constructor() {
    this.loadingSubject = new Subject<number>();
    this.isLoading = this.loadingSubject.pipe(
      startWith(0),
      scan((total, i) => Math.max(0, total + i), 0),
      map(v => v > 0),
      publish(),
      refCount());
  }
  show() {
    this.loadingSubject.next(1);
  }
  hide() {
    this.loadingSubject.next(-1);
  }
}
