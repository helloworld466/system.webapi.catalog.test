import { LOCALE_ID, StaticClassProvider } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import localeFrCa from '@angular/common/locales/fr-CA';
import localeDeDe from '@angular/common/locales/de';
import localeEsEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localeFrCa);
registerLocaleData(localeDeDe);
registerLocaleData(localeEsEs);
export class DynamicLocaleId extends String {
  constructor(protected translateService: TranslateService) {
    super();
  }
  toString() {
    return this.translateService.currentLang;
  }
}
export const localeProvider: StaticClassProvider = {
  provide: LOCALE_ID,
  useClass: DynamicLocaleId,
  deps: [TranslateService]
}
