import { TestBed } from '@angular/core/testing';
import { TokenService } from './token.service';
describe('TokenService Test cases', () => {
  let service: TokenService;
  const TOKEN_KEY = 'AccessToken';
  const USER_KEY = 'auth-user';
  const mockToken = 'test-token-value';
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [TokenService] });
    service = TestBed.inject(TokenService);
  });
  describe('Generic Validation', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('getToken()', () => {
    it('Should verify AccessToken is null when AccessToken not exist inside localStorage', () => {
      window.localStorage.removeItem(TOKEN_KEY);
      let AccessToken = service.getToken();
      expect(AccessToken).toBeNull();
    });
    it('Should verify AccessToken is not null or Undefined', () => {
      window.localStorage.setItem(TOKEN_KEY, mockToken);
      let AccessToken = service.getToken();
      expect(AccessToken).not.toBeNull();
      expect(AccessToken).not.toBeUndefined();
    });
  });
  describe('saveToken()', () => {
    it('Should verify localStorage setItem() called with token', () => {
      let spy = spyOn(window.localStorage, 'setItem');
      service.saveToken(mockToken);
      expect(spy).toHaveBeenCalledWith(TOKEN_KEY, mockToken);
    });
  });
});
