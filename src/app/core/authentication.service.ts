import { HttpClient, HttpHandler, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ConfigService } from './config.service';
@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(private configService: ConfigService, private httpClient: HttpClient) {
  }
  appendHeaders(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({
      setHeaders: {
        authorization: `Bearer ${token}`,
        'site-code': this.configService.siteCode,
        'reseller-id': this.configService.resellerId
      }
    });
  }
  handleRequest(next: HttpHandler, authReq: HttpRequest<any>) {
    return next.handle(authReq);
  }
  reload() {
    location.reload();
  }
  getNewAccessToken(): Observable<string> {
    let headers = new HttpHeaders();
    headers = headers.set('X-Requested-With', 'XMLHttpRequest');
    return this.httpClient.post<string>('/Site/Sso/GetNewAccessToken', '', { headers: headers });
  }
}
