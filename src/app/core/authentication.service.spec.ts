import { HttpClient, HttpEvent, HttpEventType, HttpHandler, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { fakeAsync, flush, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { config } from 'process';
import { of } from 'rxjs/internal/observable/of';
import { AuthService } from './authentication.service';
import { ConfigService } from './config.service';
describe('AuthService Test cases', () => {
  let service: AuthService;
  let configService: ConfigService;
  let spyConfigService: ConfigService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  beforeEach(waitForAsync(() => {
    spyConfigService = jasmine.createSpyObj('ConfigService', ['load']);
    spyConfigService.resellerId = '12';
    spyConfigService.siteCode = 'us';
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AuthService,
        {
          provide: ConfigService,
          useValue: spyConfigService
        }
      ]
    }).compileComponents();
  }));
  beforeEach(() => {
    service = TestBed.inject(AuthService);
    configService = TestBed.inject(ConfigService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });
  describe('Generic Validation', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('handleRequest', () => {
    it('Should verify handleRequest method return passed request correctly ', () => {
      // Arrange
      const url = 'http://dummy/quotes';
      const token = 'token';
      let httpReq = new HttpRequest('GET', url);
      let httpRes = new HttpResponse();
      const next: any = {
        handle: (request: HttpRequest<any>) => ({
          httpRes
        })
      };
      // Act
      let response = service.handleRequest(next, httpReq);
      // Assert
      expect(response).toBeTruthy();
    });
  });
  describe('getNewAccessToken', () => {
    it('should return valid response with new token', () => {
      const newAccessToken = 'test-1';
      let headers = new HttpHeaders();
      headers = headers.set('X-Requested-With', 'XMLHttpRequest');
      service.getNewAccessToken().subscribe(
        data => {
          expect(data).toEqual(newAccessToken)
        },
        fail
      );
      const httpReq = new HttpRequest('POST', '/Site/Sso/GetNewAccessToken', '', { headers: headers });
      let httpRequest = httpTestingController.expectOne((req: HttpRequest<any>) =>
        req.method === httpReq.method);
      expect(httpRequest.request.method).toEqual('POST');
      expect(httpRequest.request.headers).toEqual(headers);
      // Expect server to return the newAccessToken after POST
      const expectedResponse = new HttpResponse({ status: 201, statusText: 'Created', body: newAccessToken });
      httpRequest.event(expectedResponse);
    });
  });
});
