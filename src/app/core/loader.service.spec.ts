import { TestBed } from '@angular/core/testing';
import { LoaderService } from './loader.service';
describe('LoaderService Test cases', () => {
  let service: LoaderService;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [LoaderService] });
    service = TestBed.inject(LoaderService);
  });
  describe('Generic validation', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });
  });
  describe('Show', () => {
    it('Should call next method of loadingSubject', () => {
      // @ts-ignore
      let spyloadingSubject = spyOn(service.loadingSubject, 'next');
      service.show();
      expect(spyloadingSubject).toHaveBeenCalledTimes(1);
    });
  });
  describe('Hide', () => {
    it('Should call next method of loadingSubject', () => {
      // @ts-ignore
      let spyloadingSubject = spyOn(service.loadingSubject, 'next');
      service.hide();
      expect(spyloadingSubject).toHaveBeenCalledTimes(1);
    });
  });
});
