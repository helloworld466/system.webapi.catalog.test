import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpBackend, HttpClient, HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpParams, HttpRequest, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, of, throwError, pipe, EMPTY, empty, NEVER } from 'rxjs';
import { Component, Injectable, ReflectiveInjector } from '@angular/core';
import { stringify } from '@angular/compiler/src/util';
import { catchError } from 'rxjs/operators';
import { doesNotReject } from 'assert';
import { request } from 'http';
import { TokenService } from '../token.service';
import { AuthService } from '../authentication.service';
import { ConfigService } from '../config.service';
import { AuthInterceptor } from './auth-interceptor';
describe('AuthInterceptor Test cases', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let sypTokenService: any;
  let tokenService: TokenService;
  let authService: AuthService;
  let spyAuthService: AuthService;
  let spyConfigService: ConfigService;
  let authInterceptor: AuthInterceptor;
  let spyLog;
  let searchAPIEndPt: string = 'https://localhost:44381/v1/api/';
  beforeEach(waitForAsync(() => {
    spyAuthService = jasmine.createSpyObj('AuthService', ['addExtraHeaders',
      'appendHeaders', 'refreshErrorToken', 'handleRequest', 'getNewAccessToken', 'handleRequest']);
    sypTokenService = jasmine.createSpyObj('TokenService', ['getToken', 'saveToken', 'reload', 'handleRequest', 'getNewAccessToken']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: TokenService,
          useValue: sypTokenService
        },
        {
          provide: AuthService,
          useValue: spyAuthService
        },
        AuthInterceptor,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
        }
      ]
    }).compileComponents();
  }));
  beforeEach(() => {
    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    // loc = TestBed.inject(Location);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    authService = TestBed.inject(AuthService);
    tokenService = TestBed.inject(TokenService);
    authInterceptor = TestBed.inject(AuthInterceptor);
  });
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
    TestBed.resetTestingModule();
  });
  describe('Generic Validation', () => {
    it('should be created', () => {
      expect(authInterceptor).toBeTruthy();
    });
  });
  describe('intercept', () => {
    it(`Should verify getToken, saveToken, getNewAccessToken, handleRequest and appendHeaders method call happen when response status 401 bad request`, () => {
      // Arrange 
      const url = 'http://dummy/api/'
      let httpReq = new HttpRequest('GET', url);
      let httpRes = new HttpResponse();
      const next: any = {
        handle: (request: HttpRequest<any>) => ({
          open: []
        })
      };
      const token = 'token';
      const newRefreshToken = 'new-refresh-token';
      tokenService.getToken = jasmine.createSpy('getToken').and.returnValue(token);
      tokenService.saveToken = jasmine.createSpy('saveToken').and.returnValue(newRefreshToken);
      authService.appendHeaders = jasmine.createSpy('appendHeaders').and.returnValue(httpReq);
      authService.getNewAccessToken = jasmine.createSpy('getNewAccessToken').and.returnValue(of(newRefreshToken));
      httpClient.get(url).subscribe(
        res => { console.log(res); },
        x => {
          // Assert
          expect(authService.getNewAccessToken).toHaveBeenCalledTimes(1);
          expect(authService.appendHeaders).toHaveBeenCalledTimes(2);
          expect(authService.appendHeaders).toHaveBeenCalledWith(httpReq, token);
          expect(authService.appendHeaders).toHaveBeenCalledWith(httpReq, newRefreshToken);
          expect(tokenService.getToken).toHaveBeenCalledTimes(1);
          expect(tokenService.saveToken).toHaveBeenCalledTimes(1);
          expect(tokenService.saveToken).toHaveBeenCalledWith(newRefreshToken);
          expect(authService.handleRequest).toHaveBeenCalledTimes(1);
        }
      );
      // Act
      let httpRequest = httpTestingController.expectOne((req: HttpRequest<any>) =>
        req.method === httpReq.method && req.urlWithParams === httpReq.urlWithParams);
      httpRequest.error(null, { status: 401, statusText: 'Bad Request', });
    });
    it(`Should verify request is throwing other error when response status is not 401 bad request`, () => {
      // Arrange 
      const url = 'http://dummy/api/'
      let httpReq = new HttpRequest('GET', url);
      const token = 'token';
      const newRefreshToken = 'new-refresh-token';
      tokenService.getToken = jasmine.createSpy('getToken').and.returnValue(token);
      authService.appendHeaders = jasmine.createSpy('appendHeaders').and.returnValue(httpReq);
      authService.getNewAccessToken = jasmine.createSpy('getNewAccessToken').and.returnValue(of(newRefreshToken));
      httpClient.get(url).subscribe(
        res => { console.log(res); },
        (x: HttpErrorResponse) => {
          // Assert
          expect(x.status).toEqual(403);
        }
      );
      // Act
      let httpRequest = httpTestingController.expectOne((req: HttpRequest<any>) =>
        req.method === httpReq.method && req.urlWithParams === httpReq.urlWithParams);
      httpRequest.error(null, { status: 403, statusText: 'Forbidden Request', });
    });
    it(`Should verify reload method call happen when response status is 401 bad request and url has 'GetNewAccessToken' call`, () => {
      // Arrange 
      const url = 'http://dummy/GetNewAccessToken'
      let httpReq = new HttpRequest('GET', url);
      const token = 'token';
      tokenService.getToken = jasmine.createSpy('getToken').and.returnValue(token);
      authService.reload = jasmine.createSpy('reload')
      spyLog = spyOn(console, 'log');
      httpClient.get(url).subscribe(
        res => { console.log(res); },
        (x: HttpErrorResponse) => {
          // Assert
          expect(console.log).toHaveBeenCalledOnceWith('IMOnline session expired');
          expect(authService.reload).toHaveBeenCalledTimes(1);
          spyLog = null;
        }
      );
      // Act
      let httpRequest = httpTestingController.expectOne((req: HttpRequest<any>) =>
        req.method === httpReq.method && req.urlWithParams === httpReq.urlWithParams);
      httpRequest.error(null, { status: 401, statusText: 'Bad Request' });
    });
    it(`Should verify request is throwing status 401 when response status 401 and newAccessToken empty`, () => {
      // Arrange 
      const url = 'http://dummy/api/'
      let httpReq = new HttpRequest('GET', url);
      const token = 'token';
      const newRefreshToken = 'new-refresh-token';
      tokenService.getToken = jasmine.createSpy('getToken').and.returnValue(token);
      authService.appendHeaders = jasmine.createSpy('appendHeaders').and.returnValue(httpReq);
      authService.getNewAccessToken = jasmine.createSpy('getNewAccessToken').and.returnValue(of(''));
      httpClient.get(url).subscribe(
        res => { console.log(res); },
        (x: HttpErrorResponse) => {
          // Assert
          expect(x.status).toEqual(401);
        }
      );
      // Act
      let httpRequest = httpTestingController.expectOne((req: HttpRequest<any>) =>
        req.method === httpReq.method && req.urlWithParams === httpReq.urlWithParams);
      httpRequest.error(null, { status: 401, statusText: 'Bad Request', });
    });
    it(`Should verify getToken() and appendHeaders() method called at once when response status ok`, () => {
      // Arrange 
      let url = 'http://dummy/api/'
      let httpReq = new HttpRequest('GET', url);
      let token = 'token';
      let newRefreshToken = 'new-refresh-token';
      tokenService.getToken = jasmine.createSpy('getToken').and.returnValue(token);
      authService.appendHeaders = jasmine.createSpy('appendHeaders').and.returnValue(httpReq);
      authService.getNewAccessToken = jasmine.createSpy('getNewAccessToken').and.returnValue(of(newRefreshToken));
      httpClient.get(url).subscribe();
      // Act
      let httpRequest = httpTestingController.expectOne((req: HttpRequest<any>) =>
        req.method === httpReq.method && req.urlWithParams === httpReq.urlWithParams);
      httpRequest.flush('string response');
      // Assert
      expect(tokenService.getToken).toHaveBeenCalledTimes(1);
      expect(authService.appendHeaders).toHaveBeenCalledTimes(1);
      expect(authService.appendHeaders).toHaveBeenCalledWith(httpReq, token);
    });
    it(`Should not call appendHeaders() method when token passed undefined with response status ok`, () => {
      // Arrange 
      let url = 'http://dummy/api/'
      let httpReq = new HttpRequest('GET', url);
      let token = undefined;
      let newRefreshToken = 'new-refresh-token';
      tokenService.getToken = jasmine.createSpy('getToken').and.returnValue(token);
      authService.appendHeaders = jasmine.createSpy('appendHeaders').and.returnValue(httpReq);
      authService.getNewAccessToken = jasmine.createSpy('getNewAccessToken').and.returnValue(of(newRefreshToken));
      httpClient.get(url).subscribe();
      // Act
      let httpRequest = httpTestingController.expectOne((req: HttpRequest<any>) =>
        req.method === httpReq.method && req.urlWithParams === httpReq.urlWithParams);
      httpRequest.flush('string response');
      // Assert
      expect(authService.appendHeaders).not.toHaveBeenCalledTimes(1);
    });
    it(`Should not call appendHeaders() method when token passed url doesn't have /api/ word with response status ok`, () => {
      // Arrange 
      let url = 'http://dummy/check'
      let httpReq = new HttpRequest('GET', url);
      let token = 'token';
      let newRefreshToken = 'new-refresh-token';
      tokenService.getToken = jasmine.createSpy('getToken').and.returnValue(token);
      authService.appendHeaders = jasmine.createSpy('appendHeaders').and.returnValue(httpReq);
      authService.getNewAccessToken = jasmine.createSpy('getNewAccessToken').and.returnValue(of(newRefreshToken));
      httpClient.get(url).subscribe();
      // Act
      let httpRequest = httpTestingController.expectOne((req: HttpRequest<any>) =>
        req.method === httpReq.method && req.urlWithParams === httpReq.urlWithParams);
      httpRequest.flush('string response');
      // Assert
      expect(authService.appendHeaders).not.toHaveBeenCalledTimes(1);
    });
  });
});
