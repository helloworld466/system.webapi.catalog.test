import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay, finalize } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { assetUrl } from '../../../single-spa/asset-url';
import { LoaderService } from '../loader.service';
@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  private _usersJsonPath = assetUrl('users.json');
  private _carsJsonPath = assetUrl('cars.json');
  constructor(public loaderService: LoaderService) { }
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return this.handleRequests(req, next);
  }
  /**
   * Handle request's and support with mock data.
   * @param req
   * @param next
   */
  handleRequests(req: HttpRequest<any>, next: HttpHandler): any {
    const { url, method } = req;
    if (method === 'GET' && (url.endsWith('/users') || url.endsWith('/cars'))) {
      if (url.endsWith('/users')) {
        req = req.clone({
          url: this._usersJsonPath,
        });
      }
      if (url.endsWith('/cars')) {
        req = req.clone({
          url: this._carsJsonPath,
        });
      }
      this.loaderService.show();
      return next.handle(req).pipe(delay(500), finalize(() => {
        this.loaderService.hide();
      }));
    }
    if (url.endsWith('/users') && method === 'POST') {
      const { body } = req.clone();
      // assign a new uuid to new user
      body.id = uuidv4();
      return of(new HttpResponse({ status: 200, body })).pipe(delay(500));
    }
    if (url.match(/\/users\/.*/) && method === 'DELETE') {
      const empId = this.getUserId(url);
      return of(new HttpResponse({ status: 200, body: empId })).pipe(
        delay(500)
      );
    }
    // if there is not any matches return default request.
    return next.handle(req);
  }
  /**
   * Get User unique uuid from url.
   * @param url
   */
  getUserId(url: any) {
    const urlValues = url.split('/');
    return urlValues[urlValues.length - 1];
  }
}
/**
 * Mock backend provider definition for app.module.ts provider.
 */
export const fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
}
