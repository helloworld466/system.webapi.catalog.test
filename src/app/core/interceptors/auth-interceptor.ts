import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ConfigService } from '../config.service';
import { TokenService } from '../token.service';
import { AuthService } from '../authentication.service';
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private tokenService: TokenService, private authService: AuthService) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const accessToken = this.tokenService.getToken();
    if (accessToken && request.url.indexOf('/api/') !== -1) {
      request = this.authService.appendHeaders(request, accessToken);
    }
    return next.handle(request).pipe(catchError(error => {
      if (error.status === 401) {
        // check here that you are hitting API url
        if (request.url.indexOf('/api/') !== -1) {
          return this.authService.getNewAccessToken().pipe(
            switchMap((newAccessToken) => {
              if (newAccessToken) {
                this.tokenService.saveToken(newAccessToken);
                request = this.authService.appendHeaders(request, newAccessToken);
                return this.authService.handleRequest(next,request);
                // return next.handle(request);
              }
              return throwError(error);
            }));
        }
        // test here that you are hitting token refresh url
        else if (request.url.indexOf('GetNewAccessToken') !== -1) {
          console.log('IMOnline session expired');
          this.authService.reload();
          // location.reload();
        }
      }
      return throwError(error);
    }));
  }
  // getNewAccessToken(): Observable<string> {
  //   let headers = new HttpHeaders();
  //   headers = headers.set('X-Requested-With', 'XMLHttpRequest');
  //   return this.httpClient.post<string>('/Site/Sso/GetNewAccessToken', '', { headers: headers });
  // }
  // appendHeaders(request: HttpRequest<any>, token: string): HttpRequest<any> {
  //   return request.clone({
  //     setHeaders: {
  //       authorization: `Bearer ${token}`,
  //       'site-code': this.configService.siteCode,
  //       'reseller-id': this.configService.resellerId
  //     }
  //   });
  // }
}
export const authInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthInterceptor,
  multi: true
}
