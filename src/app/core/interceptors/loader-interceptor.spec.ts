import { HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { fakeAsync, TestBed, waitForAsync } from "@angular/core/testing";
import { LoaderService } from "../loader.service";
import { LoaderInterceptor } from "./loader-interceptor";
describe('LoaderInterceptor Test cases', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let loaderService: LoaderService;
    let spyLoaderService: LoaderService;
    let loaderInterceptor: LoaderInterceptor;
    let spyLog;
    beforeEach(waitForAsync(() => {
        spyLoaderService = jasmine.createSpyObj('LoaderService', ['show', 'hide']);
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                {
                    provide: LoaderService,
                    useValue: spyLoaderService
                },
                LoaderInterceptor,
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: LoaderInterceptor,
                    multi: true
                }
            ]
        }).compileComponents();
    }));
    beforeEach(() => {
        // Inject the http, test controller, and service-under-test
        // as they will be referenced by each test.
        httpClient = TestBed.inject(HttpClient);
        httpTestingController = TestBed.inject(HttpTestingController);
        loaderService = TestBed.inject(LoaderService);
        loaderInterceptor = TestBed.inject(LoaderInterceptor);
    });
    afterEach(() => {
        // After every test, assert that there are no more pending requests.
        httpTestingController.verify();
        TestBed.resetTestingModule();
    });
    describe('Generic Validation', () => {
        it('should be created', () => {
            expect(loaderService).toBeTruthy();
        });
    });
    describe('intercept', () => {
        it('check show() method called at once', () => {
            // Arrange
            httpClient.get('\dummy').subscribe();
            // Act
            const httpRequest = httpTestingController.expectOne('\dummy');
            httpRequest.flush('test', {
                status: 200,
                statusText: 'OK'
            });
            // Assert
            expect(loaderService.show).toHaveBeenCalledTimes(1);
        });
        it('check hide() method called at once', () => {
            // Arrange
            // console.log = jasmine.createSpy('log');
            httpClient.get('\dummy').subscribe();
            // Act
            const httpRequest = httpTestingController.expectOne('\dummy');
            httpRequest.flush('test', {
                status: 200,
                statusText: 'OK'
            });
            // Assert
            expect(loaderService.hide).toHaveBeenCalledTimes(1);
        });
    });
});
