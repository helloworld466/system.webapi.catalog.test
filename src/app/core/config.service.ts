import { Injectable } from '@angular/core';
import { singleSpaPropsSubject } from 'src/single-spa/single-spa-props';
import { CoreModule } from './core.module';
@Injectable()
export class ConfigService {
  locale: string;
  siteCode: string;
  environment: string;
  resellerId: string;
  load() {
    return new Promise(resolve => {
      singleSpaPropsSubject.subscribe(props => {
        this.locale = props.locale ? props.locale.toLowerCase() : '';
        this.siteCode = props.siteCode;
        this.environment = props.environment;
        this.resellerId = props.resellerId;
        resolve(true);
      }, err => console.log(err));
    });
  }
}
