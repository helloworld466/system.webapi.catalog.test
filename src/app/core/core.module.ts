import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { authInterceptorProvider } from './interceptors/auth-interceptor';
import { ConfigService } from './config.service';
import { fakeBackendProvider } from './interceptors/fake-backend-interceptor';
import { loaderInterceptorProvider } from './interceptors/loader-interceptor';
import { localeProvider } from './locale';
import { HttpService } from './http.service';
import { LoaderService } from './loader.service';
import { TokenService } from './token.service';
@NgModule({
  declarations: [
    // no declarations here, this module is for core services only
    // import this module to app.module only, never import it to any feature module
  ],
  imports: [
    // most likely will stay always empty
  ],
  providers: [
    ConfigService,
    HttpService,
    LoaderService,
    TokenService,
    {
      provide: APP_INITIALIZER,
      useFactory: (configService: ConfigService) => {
        return () => configService.load();
      },
      deps: [ConfigService],
      multi: true
    },
    fakeBackendProvider,
    loaderInterceptorProvider,
    authInterceptorProvider,
    localeProvider
  ],
  exports: [
    HttpClientModule
  ]
})
export class CoreModule { }
