import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
export interface QueryParams {
  [key: string]: string | number | boolean | Date;
}
@Injectable()
export class HttpService {
  constructor(private httpClient: HttpClient) { }
  get<TResponse>(route: string, qp: QueryParams = {}): Observable<TResponse> {
    const queryString = this.getQueryString(qp);
    return this.httpClient.get<TResponse>(`${environment.backendUrl}api/v1/${route}${queryString}`)
  }
  post<TRequest, TResponse>(route: string, data: TRequest, qp: QueryParams = {}): Observable<TResponse> {
    const queryString = this.getQueryString(qp);
    return this.httpClient.post<TResponse>(`${environment.backendUrl}api/v1/${route}${queryString}`, data)
  }
  put<TRequest, TResponse>(route: string, data: TRequest, qp: QueryParams = {}): Observable<TResponse> {
    const queryString = this.getQueryString(qp);
    return this.httpClient.put<TResponse>(`${environment.backendUrl}api/v1/${route}${queryString}`, data)
  }
  patch<TRequest, TResponse>(route: string, data: TRequest, qp: QueryParams = {}): Observable<TResponse> {
    const queryString = this.getQueryString(qp);
    return this.httpClient.patch<TResponse>(`${environment.backendUrl}api/v1/${route}${queryString}`, data)
  }
  delete<TResponse>(route: string, qp: QueryParams = {}): Observable<TResponse> {
    const queryString = this.getQueryString(qp);
    return this.httpClient.delete<TResponse>(`${environment.backendUrl}api/v1/${route}${queryString}`)
  }
  private getQueryString(qp: QueryParams): string {
    const urlQueryParams = this.buildUrlQueryParams(qp);
    return urlQueryParams.length === 0 ? '' : `?${urlQueryParams.join('&')}`;
  }
  private buildUrlQueryParams(qp: QueryParams): string[] {
    return Object.keys(qp).map((key: string) => {
      const value = qp[key];
      switch (typeof value) {
        case 'string': {
          return `${key}=${value}`;
        }
        case 'number': {
          return `${key}=${value.toString()}`;
        }
        case 'boolean': {
          return `${key}=${String(value)}`;
        }
        case 'object': {
          return `${key}=${value instanceof Date ? new Date((value as Date).setHours(0, 0, 0, 0)).toISOString().split('T')[0] : ''}`;
        }
        default: {
          return `${key}=${''}`
        }
      }
    });
  }
}
