import { Injectable } from '@angular/core';
const TOKEN_KEY = 'AccessToken';
@Injectable()
export class TokenService {
  public getToken(): string {
    return window.localStorage.getItem(TOKEN_KEY);
  }
  public saveToken(token: string) {
    window.localStorage.setItem(TOKEN_KEY, token);
  }
}
