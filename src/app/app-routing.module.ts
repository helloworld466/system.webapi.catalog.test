import { APP_BASE_HREF } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { Feature1MainPageComponent } from './feature1/feature1-main-page/feature1-main-page.component';
import { Feature1Page1Component } from './feature1/feature1-page1/feature1-page1.component';
import { Feature1Page2Component } from './feature1/feature1-page2/feature1-page2.component';
const routes: Routes = [
  { path: '', redirectTo: 'feature1', pathMatch: 'full' },
  {
    path: 'feature1',
    component: AppLayoutComponent,
    children: [
      { path: '', component: Feature1MainPageComponent },
      { path: 'page1', component: Feature1Page1Component },
      { path: 'page2', component: Feature1Page2Component }
    ]
  },
  {
    path: 'feature2',
    component: AppLayoutComponent,
    loadChildren: () => import('./feature2/feature2.module').then(m => m.Feature2Module)
  },
  { path: '**', component: EmptyRouteComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' }
  ]
})
export class AppRoutingModule { }
