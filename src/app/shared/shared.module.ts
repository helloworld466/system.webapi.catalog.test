import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxMaskModule } from 'ngx-mask'
import { TranslateModule } from '@ngx-translate/core';
// Angular Material modules
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
// ngx-bootstrap module
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ImDatePipe } from './custom-date-pipe/custom-date-pipe';
import { ImCurrencyPipe } from './custom-currency-pipe/custom-currency-pipe';
import { RouterModule } from '@angular/router';
@NgModule({
  declarations: [
    ImDatePipe,
    ImCurrencyPipe
  ],
  imports: [
    //CommonModule // uncomment if any components declared in this module need any basic Angular directives, that live in Common module
  ],
  providers: [
    // should always be empty, otherwise you may end up with multiple instances
    // of one and the same service, if you import this module into lazy-loaded module
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgSelectModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    NgxMaskModule,
    PopoverModule,
    TranslateModule,
    ImDatePipe,
    ImCurrencyPipe
  ]
})
export class SharedModule { }
