import { Inject, LOCALE_ID, OnDestroy, Pipe } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import * as _ from 'lodash-es';
@Pipe({
  name: 'imCurrency',
  pure: false
})
export class ImCurrencyPipe extends CurrencyPipe implements OnDestroy {
  private changeSubs: Subscription;
  private cache;
  private previousParams = [];
  constructor(@Inject(LOCALE_ID) locale: string,
    translate: TranslateService) {
    super(locale);
    this.changeSubs = translate.onLangChange.subscribe(e => {
      locale = e.lang;
      this.cache = null;
    });
  }
  // @ts-ignore
  transform(value: any, currencyCode?: string, display?: 'code' | 'symbol' | 'symbol-narrow' | string | boolean, digitsInfo?: string, locale?: string): string | null {
    const currentParams = [value, currencyCode, display, digitsInfo, locale];
    if (!this.cache || !_.isEmpty(_.difference(this.previousParams, currentParams))) {
      this.previousParams = currentParams;
      this.cache = super.transform(value, currencyCode, display, digitsInfo, locale);
    }
    return this.cache;
  }
  ngOnDestroy(): void {
    this.changeSubs.unsubscribe();
  }
}
