import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { By } from '@angular/platform-browser';
import { of, Subject } from 'rxjs';
import { LoaderService } from '../core/loader.service';
import { LoaderComponent } from './loader.component';
xdescribe('LoaderComponent', () => {
  let component: LoaderComponent;
  let fixture: ComponentFixture<LoaderComponent>;
  let loaderService: LoaderService;
  let loaderServiceSpy: any;
  beforeEach(waitForAsync(() => {
    loaderServiceSpy = jasmine.createSpyObj('LoaderService', ['']);
    TestBed.configureTestingModule({
      imports: [MatProgressSpinnerModule],
      declarations: [LoaderComponent],
      providers: [
        { provide: LoaderService, useValue: loaderServiceSpy }
      ]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create component loader object with valid values', () => {
    expect(component).toBeTruthy();
    expect(component.mode).toBe('indeterminate');
    expect(component.value).toBe(50);
  });
  it('should show loading spinner when loading is true', () => {
    //Arrange
    fixture.detectChanges();
    //@ts-ignore
    component.loading = of(true);
    //Act
    fixture.detectChanges();
    //Assert
    const loadingSpinner = fixture.debugElement.query(By.css('.spinner'));
    expect(loadingSpinner).toBeTruthy();
  });
  it('should show loading spinner when loading is true', () => {
    //Arrange
    fixture.detectChanges();
    //@ts-ignore
    component.loading = of(false);
    //Act
    fixture.detectChanges();
    //Assert
    const loadingSpinner = fixture.debugElement.query(By.css('.spinner'));
    expect(loadingSpinner).toBeFalsy();
  });
});
