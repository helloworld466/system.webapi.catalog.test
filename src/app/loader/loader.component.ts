import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoaderService } from '../core/loader.service';
@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnDestroy {
  mode = 'indeterminate';
  value = 50;
  loading = false;
  subscription: Subscription;
  constructor(private loaderService: LoaderService) {
    this.subscription = this.loaderService.isLoading.subscribe(loading => {
      this.loading = loading;
    });
  }
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
